package com.omar.fbd.cryptoexchange.model.datasource;

import com.omar.fbd.cryptoexchange.BuildConfig;
import com.omar.fbd.cryptoexchange.model.dbentities.CurrenciesMetaDataDBModel;
import com.omar.fbd.cryptoexchange.model.dbentities.CurrencyChartDBModel;
import com.omar.fbd.cryptoexchange.model.dbentities.CurrencyDBModel;
import com.omar.fbd.cryptoexchange.model.dbentities.CurrencyDBModel_Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import io.reactivex.Observable;


public class CurrenciesLocalDataFactory implements ICurrenciesDataSource {

    @Override
    public Observable<List<CurrencyDBModel>> getCurrencies(int pageIndex) {
        return Observable.just(SQLite.select().
                from(CurrencyDBModel.class)
                .where(CurrencyDBModel_Table.pageIndex.eq(pageIndex))
                .limit( BuildConfig.PAGE_LIMIT)
                .orderBy(CurrencyDBModel_Table.rank,true)
                .queryList());
    }

    public boolean hasValidData(int pageIndex) {
        List<CurrenciesMetaDataDBModel> l = SQLite.select().from(CurrenciesMetaDataDBModel.class).queryList();
        if (l.size() > 0) {
            long cacheTime =  l.get(0).getMilliSeconds();
            long diff = System.currentTimeMillis() - cacheTime;
            boolean validCacheTime = diff <= 1000 * 60 * 10;
            boolean hasThePage = SQLite.select().
                    from(CurrencyDBModel.class)
                    .where(CurrencyDBModel_Table.pageIndex.eq(pageIndex)).count() > 0;
            return  validCacheTime && hasThePage;
        }
        return false;
    }

    public  CurrenciesMetaDataDBModel getMetaData(){
        return SQLite.select().from(CurrenciesMetaDataDBModel.class).querySingle();
    }

    public void clear() {
        SQLite.delete(CurrencyDBModel.class);
        SQLite.delete(CurrencyChartDBModel.class);
        SQLite.delete(CurrenciesMetaDataDBModel.class);
    }
}
