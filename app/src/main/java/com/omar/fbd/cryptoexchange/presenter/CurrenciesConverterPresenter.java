package com.omar.fbd.cryptoexchange.presenter;

import android.util.Log;

import com.omar.fbd.cryptoexchange.domain.CurrenciesListUseCase;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyChartUIModel;
import com.omar.fbd.cryptoexchange.view.currencyconverter.CurrencyConverterMVP;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CurrenciesConverterPresenter extends BasePresenter implements CurrencyConverterMVP.CurrencyConverterPresenterMVP {

    private CurrencyConverterMVP.CurrencyConverterViewMVP view;
    private CurrenciesListUseCase casee;

    @Inject
    public CurrenciesConverterPresenter(CurrenciesListUseCase casee) {
        this.casee = casee;
    }

    @Override
    public void setUpView(CurrencyConverterMVP.CurrencyConverterViewMVP view) {
        this.view = view;
    }

    @Override
    public void loadCurrencies() {
        addDisposable(casee.getChartData(0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).
                        subscribe(this::dataSuccess, this::dataFailure));
    }


    private void dataFailure(Throwable throwable) {
        Log.w("", "");
    }

    private void dataSuccess(List<CurrencyChartUIModel> currencyChartUIModels) {
        assert view!= null;
        view.currenciesListLoaded(currencyChartUIModels);
    }

    @Override
    public void covert(CurrencyChartUIModel currencyChartUIModel, String amount) {
        if (amount == null ||amount.length()==0|| amount.equalsIgnoreCase("0"))
            return;
        double value = Double.parseDouble(amount);
        assert view!= null;
        view.showConversionResult(value * currencyChartUIModel.getCurrencyPriceInUSD());
    }

    @Override
    public void viewStopped() {
        stop();
    }

}
