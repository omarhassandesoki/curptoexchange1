package com.omar.fbd.cryptoexchange.model.uientities;

import android.os.Parcel;
import android.os.Parcelable;

public class CurrencyChartUIModel implements  Parcelable {

    private String currencyName;
    private Double currencyPriceInUSD;
    private boolean selected;
    private Long lastUpdated;
    private Long id;


    public CurrencyChartUIModel(){

    }

    public CurrencyChartUIModel(Long id,String currencyName, Double currencyPriceInUSD, boolean selected, Long lastUpdated) {
        this.currencyName = currencyName;
        this.currencyPriceInUSD = currencyPriceInUSD;
        this.selected = selected;
        this.lastUpdated = lastUpdated;
        this.id = id;
    }

    protected CurrencyChartUIModel(Parcel in) {
        currencyName = in.readString();
        if (in.readByte() == 0) {
            currencyPriceInUSD = null;
        } else {
            currencyPriceInUSD = in.readDouble();
        }
        selected = in.readByte() != 0;
        if (in.readByte() == 0) {
            lastUpdated = null;
        } else {
            lastUpdated = in.readLong();
        }
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
    }


    public static final Creator<CurrencyChartUIModel> CREATOR = new Creator<CurrencyChartUIModel>() {
        @Override
        public CurrencyChartUIModel createFromParcel(Parcel in) {
            return new CurrencyChartUIModel(in);
        }

        @Override
        public CurrencyChartUIModel[] newArray(int size) {
            return new CurrencyChartUIModel[size];
        }
    };

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Double getCurrencyPriceInUSD() {
        return currencyPriceInUSD;
    }

    public void setCurrencyPriceInUSD(Double currencyPriceInUSD) {
        this.currencyPriceInUSD = currencyPriceInUSD;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(currencyName);
        if (currencyPriceInUSD == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(currencyPriceInUSD);
        }
        dest.writeByte((byte) (selected ? 1 : 0));
        if (lastUpdated == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(lastUpdated);
        }
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
    }

    @Override
    public String toString() {
        return getCurrencyName();
    }
}

