package com.omar.fbd.cryptoexchange.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.omar.fbd.cryptoexchange.domain.CurrenciesListUseCase;
import com.omar.fbd.cryptoexchange.model.networkentities.CurrenciesList;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyUIModel;
import com.omar.fbd.cryptoexchange.view.currencieslist.CurrenciesListMVP;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class CurrenciesListPresenter extends BasePresenter implements CurrenciesListMVP.CurrenciesListPresenterMVP {

    private CurrenciesListUseCase casee;
    private CurrenciesListMVP.CurrenciesListViewMVP view;

    @Inject
    public CurrenciesListPresenter(CurrenciesListUseCase casee) {
        this.casee = casee;
    }

    @Override
    public void setUpView(CurrenciesListMVP.CurrenciesListViewMVP viewMVP) {
        this.view = viewMVP;
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadPage() {
        addDisposable(casee.getCurrencies(view.getCurrentPageIndex() + 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(i -> view.displayLoading())
                .doOnComplete(() -> view.hideLoading())
                .subscribe(this::dataLoadSuccess, this::handleError));
    }

    private void handleError(Throwable throwable) {
        Log.w("", "");
    }

    private void dataLoadSuccess(List<CurrencyUIModel> currenciesList) {
        assert view != null;
        view.dataReloaded(currenciesList);
        assert view != null;
        view.updateMetaData(casee.getMetaData());
    }

    @Override
    public void refresh() {

    }

    @Override
    public void converterClicked() {

    }

    @Override
    public void chartsClicked() {

    }

    @Override
    public void viewStopped() {
        stop();
    }
}
