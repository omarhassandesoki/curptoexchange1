package com.omar.fbd.cryptoexchange.model.dbentities;

import com.omar.fbd.cryptoexchange.model.db.BaseDBModel;
import com.omar.fbd.cryptoexchange.model.db.CurrenciesDB;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;

@Table(database = CurrenciesDB.class)

public class CurrenciesMetaDataDBModel extends BaseDBModel {

    @PrimaryKey
    @Column
    Integer id;

    @Column
    Long timestamp;

    @Column
    Integer numCryptocurrencies;

    public CurrenciesMetaDataDBModel()
    {

    }

    public CurrenciesMetaDataDBModel(Integer id,Long timestamp, Integer numCryptocurrencies) {
        this.timestamp = timestamp;
        this.numCryptocurrencies = numCryptocurrencies;
        this.id = id;
    }


    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getNumCryptocurrencies() {
        return numCryptocurrencies;
    }

    public void setNumCryptocurrencies(Integer numCryptocurrencies) {
        this.numCryptocurrencies = numCryptocurrencies;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getMilliSeconds() {
        return getTimestamp()*1000;
    }
}
