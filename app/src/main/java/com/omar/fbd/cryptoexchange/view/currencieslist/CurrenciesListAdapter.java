package com.omar.fbd.cryptoexchange.view.currencieslist;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.omar.fbd.cryptoexchange.R;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyUIModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrenciesListAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final int VIEW_TYPE_RESULT_ITEM = 1;
    private static final int VIEW_TYPE_LOADING = 2;

    private boolean showLoadMore = true;
    private boolean loadMoreError;

    private  List<CurrencyUIModel> items;

    public CurrenciesListAdapter(List<CurrencyUIModel> items){
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView;
        if (viewType == VIEW_TYPE_RESULT_ITEM) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.currency_item, parent, false);

            return new CurrenciesListAdapter.ViewHolder(itemView);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_item, parent, false);
            return new CurrenciesListAdapter.ProgressViewHolder(itemView);
        }
    }

    public int getItemViewType(int position) {
        if (position >= items.size())
            return VIEW_TYPE_LOADING;
        else
            return VIEW_TYPE_RESULT_ITEM;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof CurrenciesListAdapter.ViewHolder) {
            CurrenciesListAdapter.ViewHolder holder = (ViewHolder) viewHolder;
            holder.mItem = items.get(position);

            holder.currencyMarketValue.setText( holder.mItem.getMarketCap().toString());
            holder.currencyName.setText(holder.mItem.getName());
            holder.currencyUSDValue.setText(holder.mItem.getPrice().toString());
        } else if (viewHolder instanceof CurrenciesListAdapter.ProgressViewHolder) {
            CurrenciesListAdapter.ProgressViewHolder holder = (ProgressViewHolder) viewHolder;

            if(loadMoreError){
                holder.progressBar.setVisibility(View.GONE);
                holder.retryButton.setVisibility(View.VISIBLE);
                holder.retryButton.setOnClickListener(v -> {
//                    mListener.retryLoadMore();
                });
            }
            else{
                holder.progressBar.setIndeterminate(true);
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.retryButton.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size() + (showLoadMore ? 1 : 0);
    }

    public void removeLoading() {
        showLoadMore = false;
        notifyDataSetChanged();
    }

    public void setLoadMoreError(boolean loadMoreError) {
        this.loadMoreError = loadMoreError;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;

        @BindView(R.id.currency_name_value)
        TextView currencyName;

        @BindView(R.id.currency_market_value)
        TextView currencyMarketValue;

        @BindView(R.id.currency_usd_value)
        TextView currencyUSDValue;


        CurrencyUIModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }
    }

    static class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBar1)
        ProgressBar progressBar;
        @BindView(R.id.load_more_retry_button)
        Button retryButton;

        ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
