package com.omar.fbd.cryptoexchange.view.charts;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.omar.fbd.cryptoexchange.R;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyChartUIModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;

public class CompareCurrenciesRecyclerAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<CurrencyChartUIModel> items;
    private  CurrencyChecked listener;

    public  CompareCurrenciesRecyclerAdapter(List<CurrencyChartUIModel> items,CurrencyChecked listener){
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.currency_name_item, parent, false);

        return new CompareCurrenciesRecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        CompareCurrenciesRecyclerAdapter.ViewHolder holder = (CompareCurrenciesRecyclerAdapter.ViewHolder) viewHolder;
        holder.mItem = items.get(position);

        holder.currencyCheckBox.setText( holder.mItem.getCurrencyName());
        holder.currencyCheckBox.setChecked(holder.mItem.isSelected());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.currency_name_checkBox)
        CheckBox currencyCheckBox;
        CurrencyChartUIModel mItem;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnCheckedChanged(R.id.currency_name_checkBox)
        void  checkChange(CheckBox checkBox){
            listener.itemChecked(mItem,checkBox.isChecked());
//            mItem.setSelected(checkBox.isSelected());
        }

    }
}
