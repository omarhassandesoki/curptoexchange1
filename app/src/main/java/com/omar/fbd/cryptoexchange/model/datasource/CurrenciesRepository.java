package com.omar.fbd.cryptoexchange.model.datasource;

import com.omar.fbd.cryptoexchange.domain.ICurrenciesRepository;
import com.omar.fbd.cryptoexchange.model.dbentities.CurrenciesMetaDataDBModel;
import com.omar.fbd.cryptoexchange.model.dbentities.CurrencyDBModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;


public class CurrenciesRepository implements ICurrenciesRepository {

    private CurrenciesLocalDataFactory localDataFactory;
    private CurrenciesCloudDataFactory cloudDataFactory;

    @Inject
    public CurrenciesRepository(CurrenciesLocalDataFactory localDataFactory, CurrenciesCloudDataFactory cloudDataFactory) {
        this.localDataFactory = localDataFactory;
        this.cloudDataFactory = cloudDataFactory;
    }

    @Override
    public Observable<List<CurrencyDBModel>> getCurrencies(int pageIndex) {
        if(localDataFactory.hasValidData(pageIndex)){
            localDataFactory.clear();
            return localDataFactory.getCurrencies(pageIndex);
        }
        else{
            return cloudDataFactory.getCurrencies(pageIndex);
        }
    }

    @Override
    public CurrenciesMetaDataDBModel getMetaData() {
        return localDataFactory.getMetaData();
    }
}
