package com.omar.fbd.cryptoexchange.view.currencyconverter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.omar.fbd.cryptoexchange.App;
import com.omar.fbd.cryptoexchange.R;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyChartUIModel;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;

public class CurrencyConverterFragment extends DialogFragment implements CurrencyConverterMVP.CurrencyConverterViewMVP {


    @BindView(R.id.currency_amount_editText)
    EditText currencyAmountEditText;

    @BindView(R.id.currency_spinner)
    Spinner currencySpinner;

    @BindView(R.id.usd_value_textview)
    TextView usdValueTextView;

    @Inject
    CurrencyConverterMVP.CurrencyConverterPresenterMVP presenter;


    private List<CurrencyChartUIModel> currenciesList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View rootView = LayoutInflater.from(getActivity()).inflate(
                R.layout.fragment_converter, null);

        ButterKnife.bind(this, rootView);
        android.app.AlertDialog alertDialog;


        alertDialog = new android.app.AlertDialog.Builder(getActivity()).setView(rootView)
                .setTitle(R.string.currency_converter)
                .setPositiveButton(R.string.close, null).create();

        alertDialog.setOnShowListener(dialog -> alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(view -> {
            alertDialog.dismiss();

        }));
        alertDialog.show();
        presenter.loadCurrencies();
        return alertDialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        App.getNetCompInjector().inject(this);
        presenter.setUpView(this);
    }

    @Override
    public void onStop() {
        super.onStop();

        presenter.viewStopped();
    }

    @Override
    public void currenciesListLoaded(List<CurrencyChartUIModel> currencies) {
        currenciesList = currencies;
        ArrayAdapter<CurrencyChartUIModel> dataAdapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),
                android.R.layout.simple_spinner_item, currenciesList);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currencySpinner.setAdapter(dataAdapter);
    }

    @OnTextChanged(R.id.currency_amount_editText)
    protected void onTextChanged(CharSequence text) {
        presenter.covert(currenciesList.get(currencySpinner.getSelectedItemPosition()),text.toString());
    }

    @OnItemSelected(R.id.currency_spinner)
    protected void  itemSelected(int index){
        presenter.covert(currenciesList.get(index), currencyAmountEditText.getText().toString());
    }

    @Override
    public void showConversionResult(double result) {
        usdValueTextView.setText(getString(R.string.equivelent_usd, result));
    }

}
