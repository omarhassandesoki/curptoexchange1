package com.omar.fbd.cryptoexchange.domain;

import com.omar.fbd.cryptoexchange.model.dbentities.CurrenciesMetaDataDBModel;
import com.omar.fbd.cryptoexchange.model.dbentities.CurrencyDBModel;
import com.omar.fbd.cryptoexchange.model.networkentities.CurrenciesList;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyChartUIModel;

import java.util.List;

import io.reactivex.Observable;


public interface ICurrenciesRepository {
    Observable<List<CurrencyDBModel>> getCurrencies(int pageIndex);
    CurrenciesMetaDataDBModel getMetaData();
 }
