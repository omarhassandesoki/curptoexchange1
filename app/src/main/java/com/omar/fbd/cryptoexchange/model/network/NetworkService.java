package com.omar.fbd.cryptoexchange.model.network;

import com.omar.fbd.cryptoexchange.model.networkentities.CurrenciesList;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class NetworkService {

    private NetworkAPI networkAPI;
    public NetworkService(Retrofit retrofit) {
        networkAPI = retrofit.create(NetworkAPI.class);
    }
    public NetworkAPI getAPI() {
        return networkAPI;
    }

    /**
     * all the Service alls to use for the retrofit requests.
     */
    public interface NetworkAPI {

        @GET("v2/ticker/?structure=array&sort=rank")
        Observable<CurrenciesList> getCurrenciesList(@Query("limit") Integer limit
                , @Query("start") Integer start);
    }
}
