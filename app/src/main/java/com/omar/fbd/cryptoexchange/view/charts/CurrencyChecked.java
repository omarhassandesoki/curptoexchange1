package com.omar.fbd.cryptoexchange.view.charts;

import com.omar.fbd.cryptoexchange.model.uientities.CurrencyChartUIModel;

public interface CurrencyChecked {
    void itemChecked(CurrencyChartUIModel item, boolean status);
}
