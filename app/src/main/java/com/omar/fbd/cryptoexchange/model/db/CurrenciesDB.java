package com.omar.fbd.cryptoexchange.model.db;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = CurrenciesDB.NAME, version = CurrenciesDB.VERSION)
public class CurrenciesDB {
    public static final String NAME = "currencies_db";

    public static final int VERSION = 1;
}
