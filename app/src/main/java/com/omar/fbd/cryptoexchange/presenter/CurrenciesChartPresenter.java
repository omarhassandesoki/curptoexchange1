package com.omar.fbd.cryptoexchange.presenter;

import com.omar.fbd.cryptoexchange.domain.CurrenciesListUseCase;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyChartUIModel;
import com.omar.fbd.cryptoexchange.view.charts.CurrenciesCharsMVP;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CurrenciesChartPresenter extends BasePresenter implements CurrenciesCharsMVP.CurrenciesChartPresenterMVP {

    private CurrenciesCharsMVP.CurrenciesChartViewMVP view;
    private CurrenciesListUseCase casee;

    @Inject
    public CurrenciesChartPresenter(CurrenciesListUseCase casee) {
        this.casee = casee;
    }

    @Override
    public void loadData() {
        addDisposable(casee.getChartData(0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).
                        subscribe(this::chartDataLoadSuccess, this::chartDataFailure));
    }

    private void chartDataLoadSuccess(List<CurrencyChartUIModel> currencyChartUIModels) {
        assert view!= null;
        view.fillCurrenciesList((ArrayList<CurrencyChartUIModel>) currencyChartUIModels);
    }

    private void chartDataFailure(Throwable throwable) {

    }

    @Override
    public void editCurrenciesListClicked() {
    }

    @Override
    public void listEdited(ArrayList<CurrencyChartUIModel> currenciesList) {
        assert view!= null;
        view.fillCurrenciesList(currenciesList);
    }

    @Override
    public void setUpView(CurrenciesCharsMVP.CurrenciesChartViewMVP view) {
        assert view!= null;
        this.view = view;
    }

    @Override
    public void viewStopped() {
        stop();
    }

}
