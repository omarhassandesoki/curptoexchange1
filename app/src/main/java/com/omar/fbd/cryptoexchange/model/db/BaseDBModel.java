package com.omar.fbd.cryptoexchange.model.db;

import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

public class BaseDBModel extends BaseModel {
    public void save(List<? extends BaseDBModel> items) {
        for (BaseDBModel item : items) {
            if (!item.exists())
                item.save();
            else item.update();
        }
    }

    public void save(BaseDBModel item) {
        if (!item.exists())
            item.save();
        else item.update();
    }

    public void delete(BaseDBModel item) {
        if (item.exists())
            item.delete();
    }
}