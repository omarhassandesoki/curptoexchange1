package com.omar.fbd.cryptoexchange.di;

import android.app.Application;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.omar.fbd.cryptoexchange.BuildConfig;
import com.omar.fbd.cryptoexchange.domain.CurrenciesListUseCase;
import com.omar.fbd.cryptoexchange.model.datasource.CurrenciesCloudDataFactory;
import com.omar.fbd.cryptoexchange.model.datasource.CurrenciesLocalDataFactory;
import com.omar.fbd.cryptoexchange.model.datasource.CurrenciesRepository;
import com.omar.fbd.cryptoexchange.model.network.NetworkService;
import com.omar.fbd.cryptoexchange.presenter.CurrenciesChartPresenter;
import com.omar.fbd.cryptoexchange.presenter.CurrenciesConverterPresenter;
import com.omar.fbd.cryptoexchange.view.charts.CurrenciesCharsMVP;
import com.omar.fbd.cryptoexchange.view.currencieslist.CurrenciesListMVP;
import com.omar.fbd.cryptoexchange.presenter.CurrenciesListPresenter;
import com.omar.fbd.cryptoexchange.view.currencyconverter.CurrencyConverterMVP;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {


    public ApiModule() {
    }

    @Provides
    @Singleton
    Cache provideOkHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        return new Cache(application.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Cache cache) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(chain -> chain.proceed(chain.request()));

        builder.addInterceptor(chain -> {
            Request.Builder requestBuilder = chain.request().newBuilder().addHeader("Content-Type", "application/json").addHeader("Accept", "application/json").
                    addHeader("Accept-Language", "en");
            requestBuilder.addHeader("Accept-Language", "en");

            Request request = requestBuilder.build();
            return chain.proceed(request);
        });

        return builder
                .readTimeout(BuildConfig.TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(BuildConfig.TIMEOUT, TimeUnit.SECONDS)
                .build();
    }


    @Provides
    @Singleton
    NetworkService provideAPINetwork(OkHttpClient okHttpClient) {
        Retrofit.Builder requestBuilder = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

                .client(okHttpClient);
        Retrofit retrofit = requestBuilder
                .build();
        return new NetworkService(retrofit);
    }

    @Provides
    @Singleton
    CurrenciesCloudDataFactory provideCurrenciesCloudDataFactory(NetworkService networkService) {
        return new CurrenciesCloudDataFactory(networkService);
    }

    @Provides
    @Singleton
    CurrenciesRepository repository(CurrenciesCloudDataFactory c) {
        return new CurrenciesRepository(new CurrenciesLocalDataFactory(), c);
    }

    @Provides
    @Singleton
    CurrenciesListUseCase provideCurrenciesListUseCase(
            CurrenciesRepository f) {
        return new CurrenciesListUseCase(f);
    }

    @Provides
    @Singleton
    CurrenciesListMVP.CurrenciesListPresenterMVP provideCurrenciesListPresenter(
            CurrenciesListUseCase casee) {
        return new CurrenciesListPresenter(casee);
    }

    @Provides
    @Singleton
    CurrenciesCharsMVP.CurrenciesChartPresenterMVP provideCurrenciesChartPresenter(
            CurrenciesListUseCase casee) {
        return new CurrenciesChartPresenter(casee);
    }

    @Provides
    @Singleton
    CurrencyConverterMVP.CurrencyConverterPresenterMVP provideCurrencyConverterPresenter(
            CurrenciesListUseCase casee) {
        return new CurrenciesConverterPresenter(casee);
    }


}