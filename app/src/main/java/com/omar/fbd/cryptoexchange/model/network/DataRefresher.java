package com.omar.fbd.cryptoexchange.model.network;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;

public class DataRefresher {

    private static long ONE_SECOND = 1000;
    private static long ONE_MINUTE = ONE_SECOND * 60;

    public static void scheduleAlarm(Context context) {
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, DataRefreshAlramReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        long repeatingInterval = ONE_SECOND * 10;
        long triggerTime = SystemClock.elapsedRealtime() +
                repeatingInterval;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            assert alarmMgr != null;
            alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, triggerTime,
                    repeatingInterval, alarmIntent);
        } else {
            assert alarmMgr != null;
            alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, triggerTime,
                    repeatingInterval, alarmIntent);
        }
    }
}