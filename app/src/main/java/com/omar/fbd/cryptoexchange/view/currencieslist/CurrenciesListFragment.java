package com.omar.fbd.cryptoexchange.view.currencieslist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.omar.fbd.cryptoexchange.App;
import com.omar.fbd.cryptoexchange.BuildConfig;
import com.omar.fbd.cryptoexchange.view.MainActivity;
import com.omar.fbd.cryptoexchange.R;
import com.omar.fbd.cryptoexchange.model.uientities.CurrenciesMetaDataUIModel;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyUIModel;
import com.omar.fbd.cryptoexchange.view.charts.CurrenciesBarCharFragment;
import com.omar.fbd.cryptoexchange.view.currencyconverter.CurrencyConverterFragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrenciesListFragment extends Fragment implements CurrenciesListMVP.CurrenciesListViewMVP {

    private static final String ITEMS_KEY = "ITEMS_KEY";

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.retry_button)
    Button retryButton;

    @BindView(R.id.error_description_textview)
    TextView errorTextView;

    @BindView(R.id.currencies_list_recyclerView)
    RecyclerView currenciesListRecyclerView;

    @BindView(R.id.last_updated_textview)
    TextView lastUpdatedTextView;

    @BindView(R.id.currencies_count_textview)
    TextView currenciesCountTextView;

    @Inject
    CurrenciesListMVP.CurrenciesListPresenterMVP presenter;

    private boolean endlessLoadingRemoved;
    private CurrenciesListAdapter currenciesListAdapter;
    private ArrayList<CurrencyUIModel> currenciesList;

    public CurrenciesListFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState == null)
            currenciesList = new ArrayList<>();
        else currenciesList = savedInstanceState.getParcelableArrayList(ITEMS_KEY);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        currenciesListAdapter = new CurrenciesListAdapter(currenciesList);
        currenciesListRecyclerView.setAdapter(currenciesListAdapter);
        initInfiniteLoad();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        App.getNetCompInjector().inject(this);
        presenter.setUpView(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_frag_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_charts:
                ((MainActivity) Objects.requireNonNull(getActivity())).addFragment(new CurrenciesBarCharFragment());
                break;
            case R.id.action_converter:
                assert getFragmentManager() != null;
                new CurrencyConverterFragment().show(getFragmentManager(), CurrencyConverterFragment.class.getName());
                break;
        }
        return true;
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.viewStopped();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(ITEMS_KEY, currenciesList);
    }

    public void initInfiniteLoad() {
        EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) currenciesListRecyclerView.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (currenciesList.size() > 0)
                    presenter.loadPage();
            }
        };

        if (!endlessLoadingRemoved) {
            currenciesListRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

            if (currenciesList.size() == 0)
                presenter.loadPage();

        } else noMoreItems();
    }

    @Override
    public int getCurrentPageIndex() {
        return currenciesList == null ? 0 : currenciesList.size() / BuildConfig.PAGE_LIMIT;
    }

    @Override
    public int getTotalItemsCount() {
        return currenciesList == null ? 0 : currenciesList.size();
    }

    @Override
    public void dataReloaded(List<CurrencyUIModel> currenciesList) {
        this.currenciesList.addAll(currenciesList);
        currenciesListAdapter.notifyDataSetChanged();
    }

    @Override
    public void noMoreItems() {
        currenciesListAdapter.removeLoading();
        endlessLoadingRemoved = true;
    }

    @Override
    public void connectionError() {
        if (currenciesList.size() == 0) {
            errorTextView.setVisibility(View.VISIBLE);
            errorTextView.setText(R.string.error);
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void refresh() {

    }

    @Override
    public void displayLoading() {
        if (currenciesList.size() == 0)
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void updateMetaData(CurrenciesMetaDataUIModel metaDataUIModel) {
        lastUpdatedTextView.setText(getString(R.string.last_updated, new Date(metaDataUIModel.getTimestamp()).toString()));
        currenciesCountTextView.setText(getString(R.string.currencies_count, metaDataUIModel.getNumCryptocurrencies()));
    }

}
