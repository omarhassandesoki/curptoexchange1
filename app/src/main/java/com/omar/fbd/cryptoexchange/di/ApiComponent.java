package com.omar.fbd.cryptoexchange.di;

import com.omar.fbd.cryptoexchange.model.network.RefreshDataIntentService;
import com.omar.fbd.cryptoexchange.view.charts.CurrenciesBarCharFragment;
import com.omar.fbd.cryptoexchange.view.currencieslist.CurrenciesListFragment;
import com.omar.fbd.cryptoexchange.view.currencyconverter.CurrencyConverterFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface ApiComponent {
    void inject(CurrenciesListFragment currenciesListFragment);
    void inject(CurrenciesBarCharFragment charFragment);
    void inject(CurrencyConverterFragment converterFragment);
    void  inject(RefreshDataIntentService service);
}