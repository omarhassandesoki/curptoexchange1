package com.omar.fbd.cryptoexchange.model.uientities;

import android.os.Parcel;
import android.os.Parcelable;

public class CurrencyUIModel implements Parcelable {

    private Double price;
    private Double marketCap;
    private Integer rank;
    private String name;
    private Long id;

    public CurrencyUIModel() {

    }

    public CurrencyUIModel(Long id, String name, Double price, Double marketCap, Integer rank) {
        this.price = price;
        this.marketCap = marketCap;
        this.rank = rank;
        this.name = name;
    }

    protected CurrencyUIModel(Parcel in) {
        price = in.readDouble();
        marketCap = in.readDouble();
        rank = in.readInt();
        name = in.readString();
        id = in.readLong();
    }


    public static final Creator<CurrencyUIModel> CREATOR = new Creator<CurrencyUIModel>() {
        @Override
        public CurrencyUIModel createFromParcel(Parcel in) {
            return new CurrencyUIModel(in);
        }

        @Override
        public CurrencyUIModel[] newArray(int size) {
            return new CurrencyUIModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (price == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(price);
        }
        if (marketCap == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(marketCap);
        }
        if (rank == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(rank);
        }
        dest.writeString(name);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(Double marketCap) {
        this.marketCap = marketCap;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static Creator<CurrencyUIModel> getCREATOR() {
        return CREATOR;
    }
}
