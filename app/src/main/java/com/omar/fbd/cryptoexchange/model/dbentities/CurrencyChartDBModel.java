package com.omar.fbd.cryptoexchange.model.dbentities;

import com.omar.fbd.cryptoexchange.model.db.BaseDBModel;
import com.omar.fbd.cryptoexchange.model.db.CurrenciesDB;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;

@Table(database = CurrenciesDB.class)

public class CurrencyChartDBModel extends BaseDBModel {

    @Column
    @PrimaryKey
    Long id;
    @Column
    String currencyName;
    @Column
    Double currencyPriceInUSD;
    @Column
    boolean selected;
    @Column
    Long lastUpdated;

    public CurrencyChartDBModel() {

    }

    public CurrencyChartDBModel(Long id,String currencyName, Double currencyPriceInUSD, boolean selected, Long lastUpdated) {
        this.currencyName = currencyName;
        this.currencyPriceInUSD = currencyPriceInUSD;
        this.selected = selected;
        this.lastUpdated = lastUpdated;
        this.id = id;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Double getCurrencyPriceInUSD() {
        return currencyPriceInUSD;
    }

    public void setCurrencyPriceInUSD(Double currencyPriceInUSD) {
        this.currencyPriceInUSD = currencyPriceInUSD;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
