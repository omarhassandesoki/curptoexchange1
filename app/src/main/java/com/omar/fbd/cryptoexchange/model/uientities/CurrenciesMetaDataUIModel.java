package com.omar.fbd.cryptoexchange.model.uientities;

import android.os.Parcel;
import android.os.Parcelable;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;

public class CurrenciesMetaDataUIModel implements Parcelable{
    private Long timestamp;
    private Integer numCryptocurrencies;

    public CurrenciesMetaDataUIModel(){

    }

    public CurrenciesMetaDataUIModel(Long timestamp, Integer numCryptocurrencies) {
        this.timestamp = timestamp;
        this.numCryptocurrencies = numCryptocurrencies;
    }

    protected CurrenciesMetaDataUIModel(Parcel in) {
        if (in.readByte() == 0) {
            timestamp = null;
        } else {
            timestamp = in.readLong();
        }
        if (in.readByte() == 0) {
            numCryptocurrencies = null;
        } else {
            numCryptocurrencies = in.readInt();
        }
    }

    public static final Creator<CurrenciesMetaDataUIModel> CREATOR = new Creator<CurrenciesMetaDataUIModel>() {
        @Override
        public CurrenciesMetaDataUIModel createFromParcel(Parcel in) {
            return new CurrenciesMetaDataUIModel(in);
        }

        @Override
        public CurrenciesMetaDataUIModel[] newArray(int size) {
            return new CurrenciesMetaDataUIModel[size];
        }
    };

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getNumCryptocurrencies() {
        return numCryptocurrencies;
    }

    public void setNumCryptocurrencies(Integer numCryptocurrencies) {
        this.numCryptocurrencies = numCryptocurrencies;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (timestamp == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(timestamp);
        }
        if (numCryptocurrencies == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(numCryptocurrencies);
        }
    }
}
