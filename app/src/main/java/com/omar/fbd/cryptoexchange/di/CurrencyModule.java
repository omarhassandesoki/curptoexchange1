//package com.omar.fbd.cryptoexchange.di;
//
//import com.omar.fbd.cryptoexchange.domain.CurrenciesListUseCase;
//import com.omar.fbd.cryptoexchange.model.data.CurrenciesCloudDataFactory;
//import com.omar.fbd.cryptoexchange.model.data.CurrenciesRepository;
//import com.omar.fbd.cryptoexchange.model.network.NetworkService;
//
//import javax.inject.Inject;
//
//import dagger.Module;
//import dagger.Provides;
//
//@Module
//public class CurrencyModule {
//
//    @Provides
//    @Inject
//    CurrenciesCloudDataFactory provideCurrenciesCloudDataFactory(NetworkService networkService){
//        return  new CurrenciesCloudDataFactory(networkService);
//    }
//
//    @Provides
//    CurrenciesListUseCase provideCurrenciesListUseCase(
//            CurrenciesRepository loadCommonGreetingUseCase) {
//        return new CurrenciesListUseCase(loadCommonGreetingUseCase);
//    }
//}
