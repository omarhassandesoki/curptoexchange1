package com.omar.fbd.cryptoexchange.model.network;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.omar.fbd.cryptoexchange.App;
import com.omar.fbd.cryptoexchange.domain.CurrenciesListUseCase;

import javax.inject.Inject;

public class RefreshDataIntentService extends IntentService {

    @Inject
    CurrenciesListUseCase casee;

    public RefreshDataIntentService() {
        super("RefreshDataIntentService");
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public RefreshDataIntentService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        App.getNetCompInjector().inject(this);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        casee.getCurrencies(1);
    }
}
