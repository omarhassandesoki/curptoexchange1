package com.omar.fbd.cryptoexchange.model.datasource;

import com.omar.fbd.cryptoexchange.BuildConfig;
import com.omar.fbd.cryptoexchange.model.dbentities.CurrenciesMetaDataDBModel;
import com.omar.fbd.cryptoexchange.model.dbentities.CurrencyDBModel;
import com.omar.fbd.cryptoexchange.model.networkentities.CurrenciesList;
import com.omar.fbd.cryptoexchange.model.network.NetworkService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CurrenciesCloudDataFactory implements ICurrenciesDataSource {

    private NetworkService networkService;

    @Inject
    public CurrenciesCloudDataFactory(NetworkService networkService) {
        this.networkService = networkService;
    }

    @Override
    public Observable<List<CurrencyDBModel>> getCurrencies(int pageIndex) {
        return
                (networkService.getAPI().
                        getCurrenciesList(BuildConfig.PAGE_LIMIT, BuildConfig.PAGE_LIMIT * pageIndex))
                        .doOnNext(i -> new CurrenciesMetaDataDBModel(1, i.getMetadata().getTimestamp(), i.getMetadata().getNumCryptocurrencies()).save())
                        .flatMapIterable(CurrenciesList::getData)
                        .map(item -> new CurrencyDBModel(pageIndex, item.getId(),
                                item.getQuotes().getUSD().getPrice(),
                                item.getQuotes().getUSD().getMarketCap(),
                                item.getRank(),
                                item.getName(),
                                item.getLastUpdated()))
                        .toList().toObservable();
    }
}
