package com.omar.fbd.cryptoexchange.domain;

import com.omar.fbd.cryptoexchange.model.datasource.CurrenciesRepository;
import com.omar.fbd.cryptoexchange.model.dbentities.CurrenciesMetaDataDBModel;
import com.omar.fbd.cryptoexchange.model.uientities.CurrenciesMetaDataUIModel;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyChartUIModel;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyUIModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;


public class CurrenciesListUseCase {

    private ICurrenciesRepository currenciesRepository;

    @Inject
    public CurrenciesListUseCase(CurrenciesRepository currenciesRepository) {
        this.currenciesRepository = currenciesRepository;
    }

    public Observable<List<CurrencyUIModel>> getCurrencies(int pageIndex) {

        return currenciesRepository.getCurrencies(pageIndex)
                .flatMapIterable(list -> list)
                .map(item -> new CurrencyUIModel(item.getId(), item.getName(), item.getPrice(),
                        item.getMarketCap(),
                        item.getRank()))
                .toList().toObservable();
    }

    public Observable<List<CurrencyChartUIModel>> getChartData(int pageIndex) {
        return currenciesRepository.getCurrencies(pageIndex)
                .flatMapIterable(list -> list)
                .map(item -> new CurrencyChartUIModel(item.getId(), item.getName(), item.getPrice()
                        , true,
                        item.getLastUpdated()))
                .toList().toObservable();
    }

    public CurrenciesMetaDataUIModel getMetaData() {
        CurrenciesMetaDataDBModel currenciesMetaDataDBModel = currenciesRepository.getMetaData();
        return new CurrenciesMetaDataUIModel(currenciesMetaDataDBModel.getMilliSeconds(),currenciesMetaDataDBModel.getNumCryptocurrencies());
    }

}
