package com.omar.fbd.cryptoexchange.model.networkentities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Metadata {

    @SerializedName("timestamp")
    @Expose
    private Long timestamp;
    @SerializedName("num_cryptocurrencies")
    @Expose
    private Integer numCryptocurrencies;
    @SerializedName("error")
    @Expose
    private Object error;

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getNumCryptocurrencies() {
        return numCryptocurrencies;
    }

    public void setNumCryptocurrencies(Integer numCryptocurrencies) {
        this.numCryptocurrencies = numCryptocurrencies;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

}
