package com.omar.fbd.cryptoexchange.view.charts;

import com.omar.fbd.cryptoexchange.model.uientities.CurrencyChartUIModel;

import java.util.ArrayList;

public interface CurrenciesCharsMVP {
    interface CurrenciesChartViewMVP
    {
        void  fillCurrenciesList(ArrayList<CurrencyChartUIModel> currenciesList);
        void  showCurrencyNamesList(ArrayList<CurrencyChartUIModel> currenciesList);

    }

    interface CurrenciesChartPresenterMVP {
        void loadData();

        void editCurrenciesListClicked();

        void listEdited(ArrayList<CurrencyChartUIModel> currenciesList);

        void setUpView(CurrenciesCharsMVP.CurrenciesChartViewMVP view);

        void  viewStopped();
    }
}
