package com.omar.fbd.cryptoexchange.model.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DataRefreshAlramReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent i) {
        context.startService(new Intent(context, RefreshDataIntentService.class));
    }
}
