package com.omar.fbd.cryptoexchange.view.currencyconverter;

import com.omar.fbd.cryptoexchange.model.uientities.CurrencyChartUIModel;

import java.util.ArrayList;
import java.util.List;

public interface CurrencyConverterMVP {
    interface CurrencyConverterViewMVP {
        void currenciesListLoaded(List<CurrencyChartUIModel> currencies);

        void showConversionResult(double result);
    }

    interface CurrencyConverterPresenterMVP {
        void setUpView(CurrencyConverterViewMVP view);

        void loadCurrencies();

        void covert(CurrencyChartUIModel currencyChartUIModel, String amount);

        void  viewStopped();
    }
}
