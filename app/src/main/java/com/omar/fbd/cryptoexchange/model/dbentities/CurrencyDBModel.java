package com.omar.fbd.cryptoexchange.model.dbentities;

import com.omar.fbd.cryptoexchange.model.db.BaseDBModel;
import com.omar.fbd.cryptoexchange.model.db.CurrenciesDB;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;

@Table(database = CurrenciesDB.class)

public class CurrencyDBModel extends BaseDBModel {

    @Column
    @PrimaryKey
    Long id;
    @Column
    Double price;
    @Column
    Double marketCap;
    @Column
    Integer rank;
    @Column
    String name;
    @Column
    Long lastUpdated;
    @Column
    int pageIndex;

    public CurrencyDBModel() {

    }

    public CurrencyDBModel( int pageIndex, Long id,Double price, Double marketCap, Integer rank, String name,Long lastUpdated) {
        this.price = price;
        this.marketCap = marketCap;
        this.rank = rank;
        this.name = name;
        this.lastUpdated = lastUpdated;
        this.id = id;
        this.pageIndex = pageIndex;

        save();
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(Double marketCap) {
        this.marketCap = marketCap;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public int getPageIndex() {
        return pageIndex;
    }

}
