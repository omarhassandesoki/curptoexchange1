package com.omar.fbd.cryptoexchange.model.datasource;

import com.omar.fbd.cryptoexchange.model.dbentities.CurrencyDBModel;

import java.util.List;

import io.reactivex.Observable;

public interface ICurrenciesDataSource {
    Observable<List<CurrencyDBModel>> getCurrencies(int pageIndex);
}
