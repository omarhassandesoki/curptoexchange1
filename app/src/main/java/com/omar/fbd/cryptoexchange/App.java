package com.omar.fbd.cryptoexchange;

import android.app.Application;

import com.omar.fbd.cryptoexchange.di.ApiComponent;
import com.omar.fbd.cryptoexchange.di.ApiModule;
import com.omar.fbd.cryptoexchange.di.AppModule;
import com.omar.fbd.cryptoexchange.di.DaggerApiComponent;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

public class App extends Application  {

    private static ApiComponent mApiComponent;
    @Override
    public void onCreate() {
        super.onCreate();

        mApiComponent = DaggerApiComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule())
                 .build();

        FlowManager.init(new FlowConfig.Builder(this).build());
    }

    public static ApiComponent getNetCompInjector() {
        return mApiComponent;
    }
}
