package com.omar.fbd.cryptoexchange.view.currencieslist;

import com.omar.fbd.cryptoexchange.model.networkentities.Datum;
import com.omar.fbd.cryptoexchange.model.uientities.CurrenciesMetaDataUIModel;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyUIModel;

import java.util.List;

public interface CurrenciesListMVP {
    interface CurrenciesListViewMVP {
        int getCurrentPageIndex();

        int getTotalItemsCount();

        void dataReloaded(List<CurrencyUIModel> currenciesList);

        void noMoreItems();

        void connectionError();

        void refresh();

        void displayLoading();

        void hideLoading();

        void  updateMetaData(CurrenciesMetaDataUIModel metaDataUIModel);

    }

    interface CurrenciesListPresenterMVP {

        void  setUpView(CurrenciesListViewMVP viewMVP);

        void loadPage();

        void refresh();

        void  converterClicked();
        void  chartsClicked();
        void  viewStopped();

    }
}
