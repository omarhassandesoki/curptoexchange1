package com.omar.fbd.cryptoexchange.view.charts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.omar.fbd.cryptoexchange.App;
import com.omar.fbd.cryptoexchange.R;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyChartUIModel;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CurrenciesBarCharFragment extends Fragment implements CurrenciesCharsMVP.CurrenciesChartViewMVP {
    private static final String ITEMS_KEY = "ITEMS_KEY";

    private static final int EDIT_CURRENCIES = 1;

    @Inject
    CurrenciesCharsMVP.CurrenciesChartPresenterMVP presenter;
    @BindView(R.id.bar_chart)
    BarChart barChart;
    private ArrayList<CurrencyChartUIModel> data;

    public CurrenciesBarCharFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
            data = new ArrayList<>();
        else data = savedInstanceState.getParcelableArrayList(ITEMS_KEY);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_currencies_bar_char, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (data.size() == 0)
            presenter.loadData();

        initChart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        App.getNetCompInjector().inject(this);
        presenter.setUpView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();

        presenter.viewStopped();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(ITEMS_KEY, data);
    }


    @OnClick(R.id.edit_currencies)
    void edit() {
        showCurrencyNamesList(data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == EDIT_CURRENCIES) {
            presenter.listEdited(intent.getParcelableArrayListExtra(CurrencyNamesFragment.ITEMS_KEY));
        }
    }

    private void initChart() {
        barChart.setDrawGridBackground(false);
        barChart.setDrawBarShadow(false);
        barChart.getAxisRight().setEnabled(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setFitBars(true);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setEnabled(false);
    }

    private void setData() {

        ArrayList<BarEntry> yVals1 = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).isSelected()) {
                BarEntry c = new BarEntry(i, Float.parseFloat(data.get(i).getCurrencyPriceInUSD().toString()));
                yVals1.add(c);
            }
        }

        BarDataSet set1;
        set1 = new BarDataSet(yVals1, getString(R.string.crypto_currencies));
        set1.setDrawIcons(false);
        set1.setColors(ColorTemplate.MATERIAL_COLORS);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setBarWidth(0.8f);
        barChart.setData(data);
        barChart.notifyDataSetChanged();
        barChart.invalidate();
    }

    @Override
    public void fillCurrenciesList(ArrayList<CurrencyChartUIModel> currenciesList) {
        this.data = currenciesList;
        setData();
    }

    @Override
    public void showCurrencyNamesList(ArrayList<CurrencyChartUIModel> currenciesList) {
        CurrencyNamesFragment currencyNamesFragment = new CurrencyNamesFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(CurrencyNamesFragment.ITEMS_KEY, currenciesList);
        currencyNamesFragment.setArguments(bundle);
        assert getFragmentManager() != null;
        currencyNamesFragment.show(getFragmentManager(), CurrencyNamesFragment.class.getName());
        currencyNamesFragment.setTargetFragment(this, EDIT_CURRENCIES);
    }
}
