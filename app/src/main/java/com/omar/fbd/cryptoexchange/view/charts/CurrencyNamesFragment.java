package com.omar.fbd.cryptoexchange.view.charts;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.omar.fbd.cryptoexchange.R;
import com.omar.fbd.cryptoexchange.model.uientities.CurrencyChartUIModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrencyNamesFragment extends DialogFragment {

    public  static  final  String ITEMS_KEY="ITMES_KEY";

    @BindView(R.id.currency_names_recyclerView)
    RecyclerView currencyNamesRecyclerView;
    ArrayList<CurrencyChartUIModel> items;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null)
            items = getArguments().getParcelableArrayList(ITEMS_KEY);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View root = LayoutInflater.from(getActivity()).inflate(
                R.layout.fragmnet_currency_names, null);

        ButterKnife.bind(this, root);

        android.app.AlertDialog alertDialog;

        CompareCurrenciesRecyclerAdapter adapter = new CompareCurrenciesRecyclerAdapter(items, (item, status) -> {
            Log.w("",items.size()+"");
            item.setSelected(status);
        });
        currencyNamesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        currencyNamesRecyclerView.setAdapter(adapter);

        alertDialog = new android.app.AlertDialog.Builder(getActivity()).setView(root)
                .setTitle(R.string.selected_currencies)
                .setPositiveButton(R.string.ok, null).setNegativeButton(R.string.cancel, null).create();

        alertDialog.setOnShowListener(dialog -> alertDialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(view -> {

//            String key = TOP_VIEWS;
//            for (SortItem sortItem : sortItems) {
//                if (sortItem.isChecked()) {
//                    key = sortItem.getCriteria();
//                    break;
//                }
//            }
//
            Intent intent = new Intent();
            intent.putParcelableArrayListExtra(ITEMS_KEY, items);
            getTargetFragment().onActivityResult(
                    getTargetRequestCode(), 1, intent);
            alertDialog.dismiss();

        }));
        alertDialog.show();
        return alertDialog;
    }

}
