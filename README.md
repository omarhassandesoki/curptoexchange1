CurptoExchange1
- The app supports pagination, each page contains 20 currency.
- Chart page will display only top 20 available currencies.
- App can save data locally and invalidate the cache every 10 minutes.
- An alarm manager will be fired every 5 minutes to fetch latest data (Alarm manager is a bad choice, The minimum supported interval by work manager or Job scheduler is 15 minutes).